package com.mybatis.example.controllers;

import com.mybatis.example.domain.User;
import com.mybatis.example.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UsersController {
    @Autowired
    private UserMapper user;

    @RequestMapping("/lista-utenti")
    public List<User> getUsers() {
        return this.user.findAllUsers();
    }
}
